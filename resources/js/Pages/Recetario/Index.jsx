import React from "react";
import Menu from "../../Components/Menu";
import Breadcrumb from "../../Components/Breadcrumb";
import Hero from "../../Components/Hero";
import Cotizacion from "../../Components/Cotizacion";
import Subscribe from "../../Components/Subscribe";
import Footer from "../../Components/Footer";
import "./style.scss";

const Recetario = () => {
    return (
        <div className="formulario">
            <Menu></Menu>
            <Breadcrumb></Breadcrumb>
            <Hero></Hero>
            <Cotizacion></Cotizacion>
            <Subscribe></Subscribe>
            <Footer></Footer>
        </div>
    );
};

export default Recetario;
