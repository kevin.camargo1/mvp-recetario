import React from "react";
import close from "../../../imgs/close.svg";
import alertImg from "../../../imgs/alert.svg";
import closeImg from "../../../imgs/close.svg";

import { Worker } from "@react-pdf-viewer/core";
import { Viewer } from "@react-pdf-viewer/core";
import "./style.scss";
import "@react-pdf-viewer/default-layout/lib/styles/index.css";

function Index(props) {
    const [modal, setModal] = React.useState(false);

    const openModal = (e) => {
        setModal(!modal);
    };
    const deleteHandler = () => {
        props.handler();
    };
    const handleEsc = (e) => {
        if (e.keyCode === 27) {
            setModal(false);
        }
    };
    React.useEffect(() => {
        window.addEventListener("keydown", handleEsc);
    }, []);
    return (
        <>
            {props.type === "ERROR" ? (
                <div className="error">
                    <div className="error__title">¡Ha ocurrido un error!</div>
                    <div className="error__content">
                        <div className="error__content--img">
                            <img
                                width="23"
                                height="23"
                                src={alertImg}
                                alt="alert"
                            />
                        </div>
                        <div className="error__content--main">
                            No se pudo subir el archivo, te invitamos a
                            intentarlo nuevamente.
                            <br />
                            <span>(Formato permitido JPG, PDF y PNG)</span>
                        </div>
                        <div className="error__content--close">
                            <img
                                width="10"
                                heigth="10"
                                onClick={deleteHandler}
                                src={closeImg}
                                alt=""
                            />
                        </div>
                    </div>
                </div>
            ) : (
                <div className="preview">
                    <div className="preview__content">
                        <span className="preview__content--name">
                            {props.filename ?? "receta"}
                        </span>
                        <span
                            className="preview__content--view"
                            onClick={openModal}
                        >
                            Ver archivo
                        </span>
                    </div>
                    <div className="preview__footer">
                        <span
                            onClick={deleteHandler}
                            className="preview__footer--delete"
                        >
                            Eliminar
                        </span>
                    </div>
                </div>
            )}

            {modal && (
                <div class="modal">
                    <div className="modal__overlay"></div>

                    <div className="modal__content">
                        <p>Esta es la receta médica que acabas de subir.</p>
                        <div
                            className="modal__content--close"
                            onClick={openModal}
                        >
                            <img src={close} alt="" />
                        </div>
                        {props.type === "PDF" && (
                            <div>
                                <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.14.305/build/pdf.worker.min.js"></Worker>
                                <div>
                                    <Viewer fileUrl={props.img} />
                                </div>
                            </div>
                        )}
                        {props.type === "IMAGEN" && (
                            <img src={props.img} alt="" />
                        )}
                    </div>
                </div>
            )}
        </>
    );
}

export default Index;
