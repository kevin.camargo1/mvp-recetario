import React from "react";
import Subtitle from "../../Components/utils/Subtitle";
import Label from "../../Components/utils/Label";
import Terminos from "../../Components/utils/Terminos";
import ImgPreview from "../ImgPreview/Index";
import { useForm } from "react-hook-form";
import Modal from "../../Components/Modal";
import datRegions from "../../../mocks/regions.json";
import datLocales from "../../../mocks/locales.json";
import "./style.scss";

function Index() {
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm();
    const [img, setImg] = React.useState("");
    const [imgPreview, setImgPreview] = React.useState(false);
    const [imgNombre, setImgNombre] = React.useState("");
    const [imgType, setImgType] = React.useState("");
    const [btnActive, setBtnActive] = React.useState(true);
    const [dataRegions, setDataRegions] = React.useState([]);
    const [dataComuna, setDataComuna] = React.useState([]);
    const [dataLocales, setDataLocales] = React.useState([
        "No se encontró locales.",
    ]);
    const [successForm, setSuccessForm] = React.useState(false);

    const [values, setValues] = React.useState({
        nombre: "",
        rut: "",
        fecha_nacimento: "",
        direccion: "",
        email: "",
        envio_comuna: "",
        envio_region: "",
        envio_local: "",
        telefono: "",
        comentarios: "",
        medico_nombre: "",
        medico_rut: "",
        medico_comentario: "",
        receta_img: "",
    });

    const handlerActive = () => {
        setBtnActive(!btnActive);
    };
    const onSubmit = (data, e) => {
        setSuccessForm(true);
        //Meethod for send data to API
    };
    const onError = (errors, e) => console.log("Error", errors, e);

    const uploadImg = (e) => {
        const [file] = e.target.files;
        if (e.target.files[0].type.includes("image")) {
            setImgType("IMAGEN");
        } else if (e.target.files[0].type.includes("pdf")) {
            setImgType("PDF");
        } else {
            setImgType("ERROR");
        }
        setImgNombre(e.target.files[0].name);
        setImg(URL.createObjectURL(file));
        setImgPreview(true);
    };
    const selectRegion = (e) => {
        let source = [];
        datRegions[
            e.target.selectedOptions[0].getAttribute("ident")
        ].comunas.map((e) => {
            source.push(e);
        });
        setDataComuna(source);
    };

    const selectLocalRetiro = (e) => {
        let source;

        source = datLocales.filter((index) => {
            return index.nombre === e.target.value;
        });

        if (source.length >= 1) {
            setDataLocales(source[0].locales);
        } else {
            setDataLocales(["No se encontró locales."]);
        }
    };
    React.useEffect(() => {
        let source = [];
        datRegions.map((e) => {
            source.push(e.region);
        });
        setDataRegions(source);
    }, []);

    return (
        <div className="cotizacion">
            <h2>Formulario de Cotización Recetario Magistral</h2>
            <p>
                Servicio disponible con retiro en tienda y entrega máxima en 72
                horas en Región Metropolitana y 5 días hábiles en otras regiones
                desde que se realiza el pago.
            </p>
            <Subtitle>Datos del paciente:</Subtitle>
            <form
                onSubmit={handleSubmit(onSubmit, onError)}
                class="cotizacion__form"
            >
                <Label required="true">Nombre completo</Label>
                <input
                    className="cotizacion__form--input"
                    type="text"
                    placeholder="Ej: Constanza González"
                    name="nombre"
                    {...register("nombre", {
                        required: "Campo requerido",
                        minLength: {
                            value: 5,
                            message: "Mínimo 5 caracteres",
                        },
                    })}
                />
                {
                    <span className="errorinput">
                        {errors.nombre && <p>{errors.nombre.message}</p>}
                    </span>
                }

                <Label required="true">Rut</Label>
                <input
                    className="cotizacion__form--input"
                    type="text"
                    placeholder="Ej: 11.111.111-1"
                    name="rut"
                    {...register("rut", {
                        required: "Rut no es válido",
                        maxLength: {
                            value: 8,
                            message: "Debe ser 8 caracteres",
                        },
                        minLength: {
                            value: 8,
                            message: "Debe 8 caracteres",
                        },
                    })}
                />
                {
                    <span className="errorinput">
                        {errors.rut && <p>{errors.rut.message}</p>}
                    </span>
                }

                <Label required="true">Fecha de nacimiento</Label>
                <div className="cotizacion__form--date">
                    <input
                        className="cotizacion__form--input"
                        type="text"
                        maxLength="8"
                        name="dia"
                        placeholder="Día"
                        {...register("dia", {
                            required: "Fecha de nacimiento no es válido",
                            maxLength: 2,
                        })}
                    />
                    <input
                        className="cotizacion__form--input"
                        type="text"
                        maxLength="8"
                        name="mes"
                        placeholder="Mes"
                        {...register("mes", {
                            required: "Fecha de nacimiento no es válido",
                            maxLength: 2,
                        })}
                    />
                    <input
                        className="cotizacion__form--input"
                        type="text"
                        maxlength="8"
                        placeholder="Año"
                        name="anio"
                        {...register("anio", {
                            required: "Fecha de nacimiento no es válido",
                            minLength: {
                                value: 4,
                                message: "Campo inválido",
                            },
                            maxLength: {
                                value: 4,
                                message: "Campo inválido",
                            },
                        })}
                    />
                </div>
                {
                    <span className="errorinput">
                        {errors.anio && <p>{errors.anio.message}</p>}
                    </span>
                }

                <Label required="true">Dirección</Label>
                <input
                    className="cotizacion__form--input"
                    type="text"
                    name="direccion"
                    placeholder="Ej: Irarrazaval 5353"
                    {...register("direccion", {
                        required: "Dirección no válido",
                        maxLength: 120,
                    })}
                />
                {
                    <span className="errorinput">
                        {errors.direccion && <p>{errors.direccion.message}</p>}
                    </span>
                }
                <Label required="true">Email</Label>
                <input
                    className="cotizacion__form--input"
                    type="text"
                    name="email"
                    placeholder="constanza@gmail.com"
                    {...register("email", {
                        required: "Email no es válido",
                        pattern: {
                            value: /^\S+@\S+$/i,
                            message: "No es un email válido.",
                        },
                    })}
                />
                {
                    <span className="errorinput">
                        {errors.email && <p>{errors.email.message}</p>}
                    </span>
                }
                <Label required="true">Teléfono</Label>

                <input
                    className="cotizacion__form--input"
                    type="text"
                    name="telefono"
                    placeholder="Ej: 912345678"
                    {...register("telefono", {
                        required: "Teléfono no es válido",
                    })}
                />
                {
                    <span className="errorinput">
                        {errors.telefono && <p>{errors.telefono.message}</p>}
                    </span>
                }

                <Label>Comentarios</Label>
                <textarea
                    className="cotizacion__form--input"
                    rows="8"
                    cols="5"
                    type="textarea"
                    name="comentarios"
                    placeholder="¡Dejános tus comentarios!"
                    {...register("comentarios")}
                />
                <Subtitle>Local de retiro:</Subtitle>
                <Label required="true">Región</Label>
                <select
                    className="cotizacion__form--input"
                    name="envio_region"
                    {...register("envio_region", {
                        required: "Campo requerido",
                    })}
                    onChange={selectRegion}
                >
                    {dataRegions.map((item, id) => (
                        <option key={id} ident={id} value={item}>
                            {item}
                        </option>
                    ))}
                </select>
                {
                    <span className="errorinput">
                        {errors.envio_region && (
                            <p>{errors.envio_region.message}</p>
                        )}
                    </span>
                }

                <Label required="true">Comuna</Label>
                <select
                    className="cotizacion__form--input"
                    name="envio_comuna"
                    {...register("envio_comuna", {
                        required: "Campo requerido",
                    })}
                    onChange={selectLocalRetiro}
                >
                    {dataComuna.map((item, id) => (
                        <option key={id} ident={id} value={item}>
                            {item}
                        </option>
                    ))}
                </select>

                <Label required="true">Local de retiro</Label>
                <select
                    className="cotizacion__form--input"
                    name="envio_local"
                    {...register("envio_local", {
                        required: "Campo requerido",
                    })}
                >
                    {dataLocales.map((item) => (
                        <option value={item}>{item}</option>
                    ))}
                </select>

                <Subtitle>Datos del médico:</Subtitle>

                <Label required="true">Nombre completo</Label>

                <input
                    className="cotizacion__form--input"
                    type="text"
                    name="medico_nombre"
                    placeholder="Ej: Constanza González"
                    {...register("medico_nombre", {
                        required: "Nombre médico no es válido",
                    })}
                />
                {
                    <span className="errorinput">
                        {errors.medico_nombre && (
                            <p>{errors.medico_nombre.message}</p>
                        )}
                    </span>
                }

                <Label required="true">Rut</Label>
                <input
                    className="cotizacion__form--input"
                    type="text"
                    placeholder="Ej: Constanza González"
                    name="medico_rut"
                    {...register("medico_rut", {
                        required: "Rut médico no es válido",
                        maxLength: {
                            value: 8,
                            message: "Debe ser 8 caracteres",
                        },
                        minLength: {
                            value: 8,
                            message: "Debe 8 caracteres",
                        },
                    })}
                />
                {
                    <span className="errorinput">
                        {errors.medico_rut && (
                            <p>{errors.medico_rut.message}</p>
                        )}
                    </span>
                }
                <Label required="true">Comentarios</Label>
                <textarea
                    className="cotizacion__form--input"
                    rows="8"
                    cols="5"
                    type="textarea"
                    name="medico_comentario"
                    placeholder="¡Dejános tus comentarios!"
                    {...register("medico_comentario")}
                />
                {
                    <span className="errorinput">
                        {errors.medico_comentario && (
                            <p>{errors.medico_comentario.message}</p>
                        )}
                    </span>
                }
                <div className="cotizacion__submit">
                    <div className="cotizacion__submit--file">
                        <label htmlFor="file" className="btnCustom">
                            Subir receta
                        </label>
                        <input
                            name="receta_img"
                            id="file"
                            type="file"
                            placeholder="Subir receta"
                            {...register("receta_img", {
                                required: "Receta no es válido",
                            })}
                            onChange={uploadImg}
                        />
                        <p>
                            La receta se debe adjuntar en formato JPG, PNG o
                            PDF.
                        </p>
                    </div>
                    <div className="cotizacion__submit--preview">
                        {imgPreview && (
                            <div>
                                {
                                    <ImgPreview
                                        img={img}
                                        filename={imgNombre}
                                        type={imgType}
                                        handler={() => setImgPreview(false)}
                                    ></ImgPreview>
                                }
                            </div>
                        )}
                    </div>
                </div>
                <Terminos handler={() => handlerActive()}></Terminos>
                <button
                    className="btnCustom"
                    type="submit"
                    disabled={btnActive}
                >
                    Enviar receta
                </button>
            </form>
            {successForm && <Modal handler={() => setSuccessForm(false)} />}
        </div>
    );
}

export default Index;
