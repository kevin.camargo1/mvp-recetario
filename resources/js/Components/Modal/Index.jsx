import React from "react";
import close from "../../../imgs/close.svg";
import "./style.scss";
function Index(props) {
    const closeModalSuccess = () => {
        props.handler();
    };
    return (
        <div className="modalOver">
            <div className="modalSuccess scale-in-top">
                <div className="modalSuccess__header">
                    <div className="modalSuccess__header--title">
                        Tu receta ha sido recibida con éxito
                    </div>
                    <div className="modalSuccess__header--close">
                        <img
                            src={close}
                            onClick={closeModalSuccess}
                            width="21"
                            height="21"
                            alt=""
                        />
                    </div>
                </div>
                <div className="modalSuccess__body">
                    Hemos recibido tu solicitud, en un plazo de 3 horas te
                    haremos llegar la cotización a tu correo electrónico.
                    <br /> Considerar que el horario es de atención es desde
                    8:30 a 18:00.
                </div>
                <div className="modalSuccess__footer">
                    Gracias por preferir el{" "}
                    <span>Recetario Magistral de Salcobrand.</span>
                </div>
            </div>
        </div>
    );
}

export default Index;
