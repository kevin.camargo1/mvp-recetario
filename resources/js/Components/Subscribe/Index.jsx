import React from "react";
import "./style.scss";

function Index() {
    return (
        <div className="subscribe">
            <div className="subscribe__title">¡Suscríbete!</div>
            <div className="subscribe__content">
                ¡No olvides de mantenerte informado por nosotros de todo lo que
                necesitas!
            </div>
            <div className="subscribe__input">
                <input placeholder="Ingresa tu e-mail" type="text" />
                <button type="button">Suscríbete</button>
            </div>
        </div>
    );
}

export default Index;
