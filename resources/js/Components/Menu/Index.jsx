import React from "react";
import logo_preunic from "../../../imgs/logo_preunic.png";
import icon_buscar_mobile from "../../../imgs/buscar_icon_mobile.png";
import icon_cuenta_mobile from "../../../imgs/cuenta_icon_mobile.png";
import icon_carro_mobile from "../../../imgs/carro_icon_mobile.png";
import icon_menu_mobile from "../../../imgs/menu_icon_mobile.png";

import "./style.scss";

const Index = () => {
    return (
        <div>
            <div className="menu">
                <div className="menu__banner">
                    <div className="menu__banner--content">
                        <ul>
                            <li>
                                <a href="https://preunic.cl/" target="_blank">
                                    <img
                                        width="85px"
                                        src="https://d1tjllbjmslitt.cloudfront.net/assets/logo_preunic-607646650e191298ef35e278fea393c5a713eeff1f0bb908acf08abff936ad09.svg"
                                        alt="logo "
                                    />
                                </a>
                            </li>
                            <li>
                                <a
                                    href="https://salcobrand.cl/catalogos"
                                    target="_blank"
                                >
                                    Catálogos
                                </a>
                            </li>
                            <li>
                                <a
                                    href="https://salcobrand.cl/content/servicios/recetario-magistral"
                                    target="_blank"
                                >
                                    Servicios
                                </a>
                            </li>
                            <li>
                                <a
                                    href="https://salcobrand.cl/mi-salcobrand"
                                    target="_blank"
                                >
                                    Beneficios
                                </a>
                            </li>
                            <li>
                                <a
                                    href="https://salcobrand.cl/mi-salcobrand"
                                    target="_blank"
                                >
                                    <a
                                        className="visible-xs"
                                        href="tel:600 360 6000"
                                        role="button"
                                    >
                                        Venta telefónica 600 360 6000
                                        <img
                                            src="https://d1tjllbjmslitt.cloudfront.net/assets/icons/arrow-down-357e729f47cb83c16f4361227f51c0cda69bae4656082238148fd81e2996f345.svg"
                                            alt=""
                                        />
                                    </a>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="menu__main">
                    <a href="#" className="menu__main--logo">
                        <img
                            src="https://d1tjllbjmslitt.cloudfront.net/assets/logo-73fe73eb9cf65adf981684077f38a616190d7759b74439763a45b9b985fc36e5.svg"
                            width="80"
                            height="76"
                            alt="logo sb"
                        />
                    </a>
                    <div className="floating--nav">
                        <div className="floating--nav--contentmobile">
                            <ul>
                                <li>
                                    <a href="">
                                        <img src={icon_menu_mobile} alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <img src={icon_buscar_mobile} alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <img src={icon_cuenta_mobile} alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <img src={icon_carro_mobile} alt="" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="menu__main--button">
                            <button
                                type="button"
                                className="menu__main--button__item"
                            >
                                Menú de categorias
                                <span>
                                    <img
                                        src="https://d1tjllbjmslitt.cloudfront.net/assets/arrow_down_blue-171f04528978b964968afe931a48a5ac24777cdf3384a0abad1900de95b5529d.svg"
                                        alt=""
                                    />
                                </span>
                            </button>
                        </div>
                        <div className="menu__main--input">
                            <input
                                type="text"
                                placeholder="¿Qué estás buscando?"
                            />
                            <button type="button">
                                <img
                                    width="17"
                                    height="17"
                                    src="https://salcobrand.cl/packs/_/assets/images/search-icon-078f61ef3cc325274e5963262bc9f115.svg"
                                    alt=""
                                />
                            </button>
                        </div>
                        <div className="menu__main--icons">
                            <div className="menu__main--icons--location">
                                <a href="#">
                                    San bernardo
                                    <span>
                                        <img
                                            src="https://salcobrand.cl/packs/_/assets/images/icons/icon-location-f0de0e2778b9ab7e7c1ebbd819f864d3.svg"
                                            alt=""
                                        />
                                    </span>
                                </a>
                            </div>
                            <div className="menu__main--icons--login">
                                <a href="#">
                                    <img
                                        width="24"
                                        heigth="24"
                                        src="https://d1tjllbjmslitt.cloudfront.net/assets/icons/icon_account-da7be79c14ecde0f5fe3e708c2643413bd2c719235e129add039dbea4f113fd7.svg"
                                        alt=""
                                    />
                                </a>
                            </div>
                            <div className="menu__main--icons--cart">
                                <a href="#">
                                    <img
                                        src="https://d1tjllbjmslitt.cloudfront.net/assets/icons/icon_shopping_cart-972abb2cd537d57fc8b8d981b514ab21347f49422f5da0571bdff89b3d238809.svg"
                                        alt=""
                                    />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="menu__footer">
                    <div className="menu__footer--container">
                        <ul>
                            <li class="icon__mobile">
                                <a href="https://www.sbpay.cl/landing-avance-salcobrand/">
                                    <img
                                        height="16"
                                        width="16"
                                        src="https://d1tjllbjmslitt.cloudfront.net/assets/icons/icon-bill-dark-17b451a62da16806d574a5f695d3aff48bdf6e7ef7a5d69f6d99967e597e4d98.svg"
                                        alt=""
                                    />
                                    Pide tu avance
                                </a>
                            </li>
                            <li class="icon__mobile">
                                <a href="https://salcobrand.cl/shipment_tracking">
                                    <img
                                        height="16"
                                        width="13"
                                        src="https://d1tjllbjmslitt.cloudfront.net/assets/icons/icon-orders-2595d8dedd167f671603c0a74e835cabf4449248fd3750a0c12cc41f68cf3177.svg"
                                        alt=""
                                    />
                                    Sigue tu compra
                                </a>
                            </li>
                            <li>
                                <img
                                    height="18"
                                    src="https://d1tjllbjmslitt.cloudfront.net/assets/mi-salcobrand/msb_logo_header-4cb061a7f2c36ecab97d878363d5254db4559fbf133cab6fb48c23edb5617254.svg"
                                    alt=""
                                />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Index;
