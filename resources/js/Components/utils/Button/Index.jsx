import React from "react";
import "./style.scss";
function Index(props) {
    return (
        <div type={props.type} class="button">
            <button onClick={(e) => props.onClick(e)} type="button">
                {props.children}
            </button>
        </div>
    );
}

export default Index;
