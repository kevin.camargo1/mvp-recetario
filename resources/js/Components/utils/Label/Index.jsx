import React from "react";
import "./style.scss";
import arrow from "../../../../imgs/Angle-left.svg";
const Index = (props) => {
    return (
        <div className="label">
            {props.children}
            {props.required ? (
                <div className="label__required">
                    <img src={arrow} /> Requerido
                </div>
            ) : (
                ""
            )}
        </div>
    );
};

export default Index;
