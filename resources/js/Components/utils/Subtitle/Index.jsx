import React from "react";
import "./style.scss";
const Index = (props) => {
    return (
        <div className="subtitle">
            <h3 className="subtitle__title">{props.children}</h3>
            <div className="subtitle__line"></div>
        </div>
    );
};

export default Index;
