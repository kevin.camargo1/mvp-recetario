import React from "react";
import ttcc from "../../../../imgs/ttcc.pdf";
import "./style.scss";
function Index(props) {
    return (
        <div className="terminos">
            <div className="terminos__check">
                <input onClick={() => props.handler()} type="checkbox" />
                <p>
                    Acepto expresamente los
                    <a href={ttcc} target="_blank">
                        Términos y Condiciones
                    </a>
                </p>
            </div>
            <div className="terminos__content">
                <div className="terminos__content--title">Consideraciones:</div>
                <div className="terminos__content--list">
                    <ul>
                        <li>
                            Solo se puede enviar una receta por formulario, en
                            caso de requerir otra receta por favor completar un
                            nuevo formulario.
                        </li>
                        <li>La cotización tiene una vigencia de 30 días.</li>
                        <li>
                            Si su receta prescribe un activo sujeto a control
                            legal por favor dirigirse a su farmacia Salcobrand
                            más cercana para solicitar el preparado.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Index;
