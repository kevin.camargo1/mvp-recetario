import React from "react";
import "./style.scss";
function Index(props) {
    return (
        <div className="Input">
            <input
                name={props.name}
                type={props.type}
                placeholder={props.placeholder}
                onChange={(e) => props.handleChange(e)}
                /* ref={register(props.rules)} */
                /* {...props.rest} */
            />
        </div>
    );
}

export default Index;
