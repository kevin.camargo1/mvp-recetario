import React from "react";
import "./style.scss";

function Index() {
    return (
        <div className="hero">
            <div className="hero__img">
                {/*                 <img src={imgHero} alt="" />
                 */}{" "}
            </div>
            <div className="hero__content">
                <h1>Recetario magistral</h1>
                <p>
                    Realiza tu cotización a un sólo click y retira en tu
                    farmacia más cercana cuando este listo.
                </p>
            </div>
        </div>
    );
}

export default Index;
