import React from "react";
import webpay from "../../../imgs/webpay.png";
import phone from "../../../imgs/phone.svg";
import mail from "../../../imgs/mail.svg";
import facebook from "../../../imgs/Facebook.svg";
import twiter from "../../../imgs/Twitter.svg";
import preunic from "../../../imgs/Preunic.svg";

import "./style.scss";
function Index() {
    return (
        <div className="footer">
            <div className="footer__content">
                <div>
                    <div className="footer__content--item">
                        <h4>SALCOBRAND</h4>
                        <ul>
                            <li>
                                <a href="">Nuestra empresa</a>
                            </li>
                            <li>
                                <a href="">Trabaja con nosotros</a>
                            </li>
                            <li>
                                <a href="">Políticas de Privacidad</a>
                            </li>
                            <li>
                                <a href="">Términos y Condiciones</a>
                            </li>
                        </ul>
                    </div>
                    <div className="footer__content--item">
                        <h4>TARJETA SBPAY</h4>
                        <ul>
                            <li>
                                <a href="">Paga tu tarjeta</a>
                            </li>
                            <li>
                                <a href="">Estado de cuenta</a>
                            </li>
                            <li>
                                <a href="">Obten tu tarjeta</a>
                            </li>
                        </ul>
                    </div>
                    <div className="footer__content--item">
                        <h4>NUESTROS LOCALES</h4>
                        <ul>
                            <li>
                                <a href="">Ubicación y horario de locales</a>
                            </li>
                            <li>
                                <a href="">Servicios</a>
                            </li>
                            <li>
                                <a href="">Catalogos</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="footer__content--item">
                    <div>
                        <h4>SERVICIO AL CLIENTE</h4>
                        <ul>
                            <li>
                                <a href="">Ayuda y Preguntas Frecuentes </a>
                            </li>
                            <li>
                                <a href="">Consulta un químico farmacéutico</a>
                            </li>
                            <li>
                                <a href="">Seguimiento de la compra</a>
                            </li>
                            <li>
                                <a href="">Cambios y devoluciones</a>
                            </li>
                            <li>
                                <a href="">Plazos y costos de entrega</a>
                            </li>
                            <li>
                                <a href="">Boleta Electrónica</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h4>MI SALCOBRAND</h4>
                        <ul>
                            <li>
                                <a href="">Beneficios del programa </a>
                            </li>
                            <li>
                                <a href="">Inscribete en Mi Salcobrand</a>
                            </li>
                            <li>
                                <a href="">Productos</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="footer__content--item">
                    <div>
                        <h4>INFORMACIÓN REGLAMENTARIA</h4>
                        <ul>
                            <li>
                                <a href="">Documentos y decretos </a>
                            </li>
                            <li>
                                <a href="">Bases legales</a>
                            </li>
                            <li>
                                <a href="">Medicamentos de venta directa</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h4>BENEFICIOS</h4>
                        <ul>
                            <li>
                                <a href="">Caja los Andes </a>
                            </li>
                            <li>
                                <a href="">Colmena </a>
                            </li>
                            <li>
                                <a href="">Consalud</a>
                            </li>
                            <li>
                                <a href="">Banmedica</a>
                            </li>
                            <li>
                                <a href="">Convenios</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <h4>CENTRO DE BIENESTAR</h4>
                        <ul>
                            <li>
                                <a href="">Centro </a>
                            </li>
                            <li>
                                <a href="">Cursos </a>
                            </li>
                            <li>
                                <a href="">Inscribirse</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="footer__content--item">
                    <div className="item__contacto">
                        <h4>CONTACTO</h4>
                        <ul>
                            <li>
                                <a href="">
                                    <img src={phone} alt="" />
                                    <span>600 360 6000</span>
                                </a>
                            </li>
                            <li>
                                <a href="mailto:atencionclientes@sb.cl">
                                    <img src={mail} alt="" />
                                    atencionclientes@sb.cl
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="item__emergencia">
                        <h4>Emergencias toxicológica</h4>
                        <p>CITUC +56 2 2635 3800</p>
                    </div>
                    <div className="item__emergencia">
                        <h4>Salud responde</h4>
                        <p>600 360 7777</p>
                    </div>
                    <div>
                        <div className="item__rss">
                            <a href="https://www.facebook.com/Salcobrand">
                                <img src={facebook} alt="" />
                            </a>
                            <a href="https://twitter.com/Salcobrand">
                                <img src={twiter} alt="" />
                            </a>
                        </div>
                    </div>
                    <div className="item__preunic">
                        <a href="">
                            <img src={preunic} alt="prounic" />
                        </a>
                    </div>
                </div>
            </div>
            <div className="footer__imgs">
                <div className="footer__imgs--items">
                    <img src={webpay} alt="medios de pago" />
                </div>
                <div className="footer__imgs--credits">
                    © SALCOBRAND TODOS LOS DERECHOS RESERVADOS | GENERAL
                    VELÁSQUEZ 9981, SAN BERNARDO.
                </div>
            </div>
        </div>
    );
}

export default Index;
