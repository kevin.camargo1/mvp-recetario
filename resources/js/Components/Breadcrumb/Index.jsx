import React from "react";
import "./style.scss";

const Index = () => {
    return (
        <div className="bread">
            <div className="bread__content">
                Inicio
                <span className="bread__content--icon">{">"}</span>
                <span className="bread__content--child">
                    Recetario magistral
                </span>
            </div>
        </div>
    );
};

export default Index;
