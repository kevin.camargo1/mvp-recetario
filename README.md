## MVP Recetario Magistral Online

El MVP fue desarrolado para gestionar las recetas de manera online.

-   [Laravel](https://laravel.com/docs/).
-   [React](https://reactjs.org/).
-   [Vite](https://vitejs.dev/guide/).
-   [Sass](https://sass-lang.com/documentation/).

## Desplegar proyecto

```console

composer install
cp .env.example .env
php artisan key:generate
npm install
npm run dev
php artisan serve

```

## Preview

<p align="center">
<a href="https://ibb.co/9N23YN5"><img src="https://i.ibb.co/TkRqhk3/screencapture-127-0-0-1-8000-2022-08-25-10-52-34-1.png" alt="screencapture-127-0-0-1-8000-2022-08-25-10-52-34-1" border="0"></a>
</p>
<p align="center">
<a href="https://ibb.co/vh5jr1b"><img src="https://i.ibb.co/1MVmpZ3/screencapture-127-0-0-1-8000-2022-08-25-10-51-00.png" alt="screencapture-127-0-0-1-8000-2022-08-25-10-51-00" border="0"></a>
</p>

-
